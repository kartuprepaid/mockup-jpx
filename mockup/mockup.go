package mockup

import (
	"mockup-jpx/models"
	"mockup-jpx/constants"
)

func GetResponseSuccesLogin() models.MLoginRes {
	return models.MLoginRes{
		Respon : models.Respon{
			Code: constants.CODE_SUCCES ,
			Message: constants.MSG_SUCCES,
			ImageUri: constants.URI_SUCCES,
		},
	}
}

func GetResponseFailedLogin() models.MLoginRes {
	return models.MLoginRes{
		Respon : models.Respon{
			Code: constants.CODE_FAILED ,
			Message: constants.MSG_FAILED,
			ImageUri: constants.URI_FAILED,
		},
	}
}


func GetAccount(resp models.BalanceReq) models.Response  {
	return models.Response{
		Respon : models.Respon{
			Code: constants.CODE_SUCCES ,
			Message: constants.MSG_SUCCES,
			ImageUri: constants.URI_SUCCES,
		},
		Content: resp ,
	}
}

func GetHistorySucces() models.ResponsContent  {

	resp := []models.HistoryTransaction{
		models.HistoryTransaction{
			Id  			:1,
			TransactionType  	:"PAY",
			ProductName 		:"PLN",
			InvNumber		:"123456",
			TransactionDate 	:"01/09/2017 09:00:01",
			BillId			:"123456",
			BillAmount		:1000000,
			PayAmount    		:1000000,
			PoinReward 		:1100,
		},
		models.HistoryTransaction{
			Id  			:2,
			TransactionType  	:"PAY",
			ProductName 		:"PDAM",
			InvNumber		:"123457",
			TransactionDate 	:"01/09/2017 09:10:01",
			BillId			:"123456",
			BillAmount		:500000,
			PayAmount    		:500000,
			PoinReward 		:100,
		},
		models.HistoryTransaction{
			Id  			:3,
			TransactionType  	:"PURCHASE",
			ProductName 		:"PULSA",
			InvNumber		:"123458",
			TransactionDate 	:"01/09/2017 09:15:01",
			BillId			:"087773770907",
			BillAmount		:50000,
			PayAmount    		:50000,
			PoinReward 		:100,
		},
	}
	return models.ResponsContent{
		Respon : models.Respon{
			Code: constants.CODE_SUCCES ,
			Message: constants.MSG_SUCCES,
			ImageUri: constants.URI_SUCCES,
		},
		Content: resp ,
	}
}

func GetBalanceSucces() models.Response {

	resp:= models.Account{
		AccountId : "123456",
		Balance   : 1000000,
		Poin      : 1500,
		RateStar  : 5,
		Name      : "IKetut Gunawan",
	}
	return models.Response{
		Respon : models.Respon{
			Code: constants.CODE_SUCCES ,
			Message: constants.MSG_SUCCES,
			ImageUri: constants.URI_SUCCES,
		},
		Content: resp ,
	}
}


func GetHistoryFailed() models.ResponsContent  {

	resp := []models.HistoryTransaction{
	}
	return models.ResponsContent{
		Respon : models.Respon{
			Code: constants.CODE_FAILED ,
			Message: constants.MSG_FAILED,
			ImageUri: constants.URI_FAILED,
		},
		Content: resp ,
	}
}

func GetBalanceFailed() models.Response {

	resp:= models.Account{

	}
	return models.Response{
		Respon : models.Respon{
			Code: constants.CODE_FAILED ,
			Message: constants.MSG_FAILED,
			ImageUri: constants.URI_FAILED,
		},
		Content: resp ,
	}
}