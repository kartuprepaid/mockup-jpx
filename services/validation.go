package services

import (
	"github.com/astaxie/beego/logs"
	"mockup-jpx/utils"
	"fmt"
)

func Validation(body []byte , token string,req interface{}, okCh chan bool ) {
	okChain := make(chan bool)
	logs.Info("Validation ")
	fmt.Println(token)
	go utils.JsonUnmarshalValidationReq(body, req, okChain)
	if <-okChain  {
		logs.Info("Succes")
		okCh <- true
	}
	okCh <- false
	logs.Info("Done")
}

