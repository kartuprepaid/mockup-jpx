package utils

import (
	"github.com/astaxie/beego/validation"
	"github.com/astaxie/beego/logs"
	"encoding/json"
)

func JsonUnmarshalValidationReq(b []byte,  req interface{}, ok chan bool)  {
	logs.Info("Byte ",string(b))
	if err:=json.Unmarshal(b, req);err == nil  {
		valid := validation.Validation{}
		logs.Info("Validation REQ ",req)
		b, err := valid.Valid(req)
		logs.Info("Validate Json : [%v] - Err : [%v]",b, err)
		if err == nil &&  b {
			ok <- true
			return
		}
	}
	ok <- false
	return
}

