// @APIVersion 1.0.0
// @Title CashBack Track Service
// @Description CashBack Tracking & Analitic Services
// @Contact iketut.gunawan@gmail.com
// @TermsOfServiceUrl http://dimogroup.co.id/
// @License Apache 2.0
// @LicenseUrl http://www.apache.org/licenses/LICENSE-2.0.html
package routers

import (
	"github.com/astaxie/beego"
	"mockup-jpx/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	ns := beego.NewNamespace("/0.1",

		beego.NSNamespace("/jpx",
			beego.NSRouter("/login", &controllers.MockupController{}, "post:Login"),
			beego.NSRouter("/balance", &controllers.MockupController{}, "post:GetBalance"),
			beego.NSRouter("/history", &controllers.MockupController{}, "post:GetHistory"),
		),

	)
	beego.AddNamespace(ns)
}
