package constants

// Predefined model error codes.
const (
	ErrNone     = 0
	ErrDatabase = -1
	ErrSystem   = -2
	ErrDupRows  = -3
	ErrNotFound = -4
	APPROVE = 0
	FAILED  = 1
	NOTE_APPROVE = "BERHASIL"
	NOTE_FAILED = "GAGAL"
	////////////////////////////////////
	CODE_SUCCES = 200
	MSG_SUCCES = "Successful"
	CODE_INVALIDJSON = 400
	MSG_INVALIDJSON = "Invalid JSON"
	CODE_CREDEXP = 401
	MSG_CREDEXP = "Credentials expired"
	CODE_DATANOTFOUND = 402
	MSG_DATANOTFOUND = "Data not found"
	CODE_UPLOADFAILED = 403
	MSG_UPLOADFAILED = "Upload unsuccessful, document type unsupported"
	CODE_VERIFYFAILED = 404
	MSG_VERIFYFAILED = "Invalid verification code"
	CODE_EXPVERIFY = 405
	MSG_EXPVERIFY = "Verification code has expired"

	CODE_INVALIDOLDPIN = 406
	MSG_INVALIDOLDPIN  = "Invalid old PIN"

	CODE_INVALIDPIN = 407
	MSG_INVALIDPIN  = "Invalid PIN"

	CODE_ALREADYEXISTS = 409
	MSG_ALREADYEXISTS  = "Data already exists"

	CODE_ERRDB = 507
	MSG_ERRDB = "Error with database"
	CODE_ERR = 509
	MSG_ERR = "Error"
	////////////////////////////////////
	CONSUL_HealthAny      = "any"
	CONSUL_HealthPassing  = "passing"
	CONSUL_HealthWarning  = "warning"
	CONSUL_HealthCritical = "critical"
	CONSUL_HealthMaint    = "maintenance"
	CREATEBY = "MOBIL-API-GATEWAY"
	ROLE = "MOBILE"

	CODE_FAILED = 100
	MSG_FAILED = "FAILED"
	URI_SUCCES = "http://www.freeiconspng.com/uploads/success-icon-19.png"
	URI_FAILED = "https://www.shareicon.net/data/128x128/2017/02/24/879485_green_512x512.png"

	//param harus huruf kecil
	//PARAM_XXX = ""
)

