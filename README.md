## Test 

### Login 
Url : http://localhost:8080/0.1/jpx/login
Method : POST 
Body : 
{ 
	"userid" : "1234",
	"password": "1234" ,
	"position" :"1234,1234",
	"devicetoken":"12345678"
}

### History 
Url : http://localhost:8080/0.1/jpx/history
Method : POST 
Body : 
{
"devicetoken" : "1234"
}



Response Body : 



{
    "code": 200,
    "message": "Successful",
    "imageuri": "http://www.freeiconspng.com/uploads/success-icon-19.png",
    "Content": [
        {
            "id": 1,
            "transactiontype": "PAY",
            "productname": "PLN",
            "invnumber": "123456",
            "transactiondate": "01/09/2017 09:00:01",
            "billid": "123456",
            "billamount": 1000000,
            "payamount": 1000000,
            "poinreward": 1100
        },
        {
            "id": 2,
            "transactiontype": "PAY",
            "productname": "PDAM",
            "invnumber": "123457",
            "transactiondate": "01/09/2017 09:10:01",
            "billid": "123456",
            "billamount": 500000,
            "payamount": 500000,
            "poinreward": 100
        },
        {
            "id": 3,
            "transactiontype": "PURCHASE",
            "productname": "PULSA",
            "invnumber": "123458",
            "transactiondate": "01/09/2017 09:15:01",
            "billid": "087773770907",
            "billamount": 50000,
            "payamount": 50000,
            "poinreward": 100
        }
    ]
}

### Balance  
Url : http://localhost:8080/0.1/jpx/balance
Method : POST 
Body : 
{
"devicetoken" : "1234"
}

Response Body : 

{
    "code": 200,
    "message": "Successful",
    "imageuri": "http://www.freeiconspng.com/uploads/success-icon-19.png",
    "Content": {
        "AccountId": "123456",
        "Balance": 1000000,
        "Poin": 1500,
        "RateStar": 5,
        "Name": "IKetut Gunawan"
    }
}
