package models

type HistoryReq struct {
	Devicetoken string `valid:"Required";json:"devicetoken"`
}

type BalanceReq struct {
	Devicetoken string  `valid:"Required";json:"devicetoken"`
}