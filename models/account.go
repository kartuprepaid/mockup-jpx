package models


type Account struct {
	AccountId string 	`valid:"Required";json:"accountid"`
	Balance   int		`valid:"Required";json:"balance"`
	Poin      int		`valid:"Required";json:"poin"`
	RateStar  int		`valid:"Required";json:"ratestar"`
	Name      string	`valid:"Required";json:"name"`
}
