package models


type HistoryTransaction struct {
	Id  			int     `json:"id"`
	TransactionType  	string  `json:"transactiontype"`
	ProductName 		string	`json:"productname"`
	InvNumber		string	`json:"invnumber"`
	TransactionDate 	string  `json:"transactiondate"` //DD/MM/YYYY HH:mm:ss
	BillId			string  `json:"billid"`
	BillAmount		int     `json:"billamount"`
	PayAmount    		int     `json:"payamount"`
	PoinReward 		int     `json:"poinreward"`
}
