package controllers

import (

	"github.com/astaxie/beego"
	"mockup-jpx/mockup"
)

// Operations about Login
type LoginController struct {
	beego.Controller
}

// @Title Create
// @Description create object
// @Param	body		body 	models.MLoginReq	true		"The object content"
// @Success 200 {string} models.MLoginRes
// @Failure 403 body is empty
// @router / [post]
func (o *LoginController) Login() {
	//var ob models.MLoginReq
	//json.Unmarshal(o.Ctx.Input.RequestBody, &ob)
	o.Data["json"] = mockup.GetResponseSuccesLogin()
	o.ServeJSON()
}
