package controllers

import (
	"mockup-jpx/models"
	"github.com/astaxie/beego"
	"mockup-jpx/mockup"
	"mockup-jpx/services"
)

// Operations about Login
type MockupController struct {
	beego.Controller
}

// @Title Create
// @Description create object
// @Param	body		body 	models.MLoginReq	true		"The object content"
// @Success 200 {string} models.MLoginRes
// @Failure 403 body is empty
// @router / [post]
func (c *MockupController) Login() {
	var req models.MLoginReq
	//json.Unmarshal(o.Ctx.Input.RequestBody, &ob)
	okChain := make(chan bool)
	go services.Validation(c.Ctx.Input.RequestBody,"",&req, okChain)
	res := mockup.GetResponseFailedLogin()
	if <- okChain {
		res = mockup.GetResponseSuccesLogin()
	}
	c.Data["json"] =  res
	c.ServeJSON()
}



// @Title Create
// @Description Get Account
// @Param	body		body 	models.BalanceReq	true		"The object content"
// @Success 200 {string} models.Response
// @Failure 403 body is empty
// @router / [post]
func (c *MockupController) GetBalance() {
	var req models.BalanceReq
	//json.Unmarshal(o.Ctx.Input.RequestBody, &ob)
	okChain := make(chan bool)
	go services.Validation(c.Ctx.Input.RequestBody,c.Ctx.Input.Header("token"),&req, okChain)
	res := mockup.GetBalanceFailed()
	if <- okChain {
		res = mockup.GetBalanceSucces()
	}
	c.Data["json"] =  res
	c.ServeJSON()
}




// @Title Create
// @Description Get History
// @Param	body		body 	models.HistoryReq	true		"The object content"
// @Success 200 {string} models.Response
// @Failure 403 body is empty
// @router / [post]
func (c *MockupController) GetHistory() {
	var req models.HistoryReq
	//json.Unmarshal(o.Ctx.Input.RequestBody, &ob)
	okChain := make(chan bool)
	go services.Validation(c.Ctx.Input.RequestBody,c.Ctx.Input.Header("token"),&req, okChain)
	res := mockup.GetHistoryFailed()
	if <- okChain {
		res = mockup.GetHistorySucces()
	}
	c.Data["json"] =  res
	c.ServeJSON()
}
